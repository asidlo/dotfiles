set runtimepath^="/Users/b004917/.vim_go_runtime"

source /Users/b004917/.vim_go_runtime/vimrc/basic.vim
source /Users/b004917/.vim_go_runtime/vimrc/filetypes.vim
source /Users/b004917/.vim_go_runtime/vimrc/plugins.vim
source /Users/b004917/.vim_go_runtime/vimrc/extended.vim

try
  source /Users/b004917/.vim_go_runtime/custom_config.vim
catch
endtry
