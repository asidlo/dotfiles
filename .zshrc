# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/b004917/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

POWERLEVEL9K_MODE="nerdfont-complete"
ZSH_THEME="powerlevel9k/powerlevel9k"
#POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
#POWERLEVEL9K_SHORTEN_STRATEGY="NONE"
#POWERLEVEL9K_SHORTEN_DELIMITER="."
#POWERLEVEL9K_DISABLE_RPROMPT=true

#ZSH_THEME="agnoster"
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
POWERLEVEL9K_SHORTEN_DELIMITER=""
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=''
POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR=''
POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR='|'
POWERLEVEL9K_RIGHT_SUBSEGMENT_SEPARATOR=''
POWERLEVEL9K_ANACONDA_BACKGROUND='clear'
POWERLEVEL9K_ANACONDA_FOREGROUND='green'
POWERLEVEL9K_VIRTUALENV_BACKGROUND='clear'
POWERLEVEL9K_VIRTUALENV_FOREGROUND='magenta'
POWERLEVEL9K_GO_FOREGROUND='clear'
POWERLEVEL9K_GO_BACKGROUND='clear'
POWERLEVEL9K_NODE_VERSION_FOREGROUND='green'
POWERLEVEL9K_NODE_VERSION_BACKGROUND='clear'
POWERLEVEL9K_KUBECONTEXT_FOREGROUND='cyan'
POWERLEVEL9K_KUBECONTEXT_BACKGROUND='clear'
POWERLEVEL9K_DOCKERMACHINE_FOREGROUND='red'
POWERLEVEL9K_DOCKERMACHINE_BACKGROUND='clear'
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="%F{blue}\u256D\u2500%F{white}"
POWERLEVEL9K_MULTILINE_SECOND_PROMPT_PREFIX="%F{blue}\u2570\uf460%F{white} "
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(root_indicator dir dir_writable_joined context docker_machine anaconda virtualenv vcs)
#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(root_indicator dir dir_writable_joined context go_version node_version kubecontext docker_machine anaconda vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(command_execution_time background_jobs_joined)
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND="clear"
POWERLEVEL9K_VCS_CLEAN_BACKGROUND="clear"
POWERLEVEL9K_VCS_CLEAN_FOREGROUND='green'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND="clear"
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND="yellow"
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND="yellow"
POWERLEVEL9K_DIR_HOME_BACKGROUND="clear"
POWERLEVEL9K_DIR_HOME_FOREGROUND="blue"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND="clear"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="blue"
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_BACKGROUND="clear"
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND="red"
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND="clear"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="white"
POWERLEVEL9K_ROOT_INDICATOR_BACKGROUND="red"
POWERLEVEL9K_ROOT_INDICATOR_FOREGROUND="white"
POWERLEVEL9K_STATUS_OK_BACKGROUND="clear"
POWERLEVEL9K_STATUS_OK_FOREGROUND="green"
POWERLEVEL9K_STATUS_ERROR_BACKGROUND="clear"
POWERLEVEL9K_STATUS_ERROR_FOREGROUND="red"
POWERLEVEL9K_TIME_BACKGROUND="clear"
POWERLEVEL9K_TIME_FOREGROUND="cyan"
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='clear'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='magenta'
POWERLEVEL9K_BACKGROUND_JOBS_BACKGROUND='clear'
POWERLEVEL9K_BACKGROUND_JOBS_FOREGROUND='green'

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode brew docker docker-compose jhipster jsontools ng npm osx vagrant)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

WORKSPACE=~/Workspace
ANACONDA_HOME=~/anaconda3
JAVA9_HOME=`/usr/libexec/java_home -v 9`
JAVA8_HOME=`/usr/libexec/java_home -v 1.8.0_144`
JAVA8_172_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_172.jdk/Contents/Home
JAVA6_HOME=`/usr/libexec/java_home -v 1.6.0_65-b14-468`
GATLING_HOME=/usr/local/opt/gatling-charts-highcharts-bundle-2.3.1
MONGOOSE_HOME=/Applications/Mongoose.app/Contents/MacOS
NGINX_HOME=/usr/local/etc/nginx

function setJava8 {
    export JAVA_HOME=$JAVA8_172_HOME
}
function setJava9 {
    export JAVA_HOME=$JAVA9_HOME
}
function setJava6 {
    export JAVA_HOME=$JAVA6_HOME
}

function curlFormat {
    curl $@ | jq '.'
}
function getDockerNames {
    docker ps -a --format "{{.Names}}"
}
function notebook {
    jupyter notebook $@
}
function get_abs_filename() {
  # $1 : relative filename
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}
function openIntellij() {
    local file=$(get_abs_filename $1)
    shift
    idea $file $@ &
}

alias ide="openIntellij"
alias ll="ls -Gltr"
alias lsh="ls -a | grep '^\.'"
alias pls="ls -Gm"
alias lla="ls -Galtr"
alias lsa="ls -Gatr"
alias cls="clear"
alias tree="tree -L 3 -C"
alias vi="vim -u ~/.vimrc.go"
alias work="cd $WORKSPACE && vi $WORKSPACE"
alias jshell="setJava9 && jshell && setJava8"
#alias java="setJava8 && java"
alias doc="cd ~/Documents"
alias nb="notebook $@"
alias copy="pbcopy"
alias cb="pbcopy"
alias gs="git status"
alias mongoose="Mongoose &"
alias nginx="nginxService"
alias server="python -m http.server $1"
alias cytoscape="/Applications/Cytoscape_v3.6.0/cytoscape.sh"
#alias curl="curlFormat $@"
#alias dsstudio="docker run -e DS_LICENSE=accept -p 9091:9091 -d --name dsstudio datastax/dse-studio"
#alias cassandra="docker run --name cassandra -dp 9042:9042 cassandra"
#alias mongodb="docker run --name mongo -dp 27017:27017 mongo"
#alias zipkin="docker run --name zipkin -dp 9411:9411 openzipkin/zipkin"
#alias kafka="docker run -d --name kafka --rm -it \
#           -p 2181:2181 -p 3030:3030 -p 8081:8081 \
#           -p 8082:8082 -p 8083:8083 -p 9092:9092 \
#           -e ADV_HOST=127.0.0.1 \
#           landoop/fast-data-dev"
#alias mysql="docker run -e MYSQL_ROOT_PASSWORD=password -dp 3306:3306 --name mysql mysql"
alias uao="unzip $1 && open $(echo $1 | cut -d"." -f1)/build.gradle"
alias dockernames="getDockerNames"
alias sbcass="echo password = solochef && echo expire date = 04-30-2018 && ssh chefsolo@11.208.32.228"
alias sbcass2="echo password = solochef && echo expire date = 05-22-2018 && ssh chefsolo@11.208.33.81"
alias sbdoc1="echo password = solochef && echo expire date = 04-30-2018 && ssh chefsolo@11.208.33.65"
alias sbdoc2="echo password = solochef && echo expire date = 04-30-2018 && ssh chefsolo@11.208.32.204"
alias sbdoc3="echo password = solochef && echo expire date = 04-30-2018 && ssh chefsolo@11.208.32.197"
alias sbkube1="echo password = solochef && echo expire date = 04-31-2018 && ssh chefsolo@11.208.33.78"
alias sbkube2="echo password = solochef && echo expire date = 04-31-2018 && ssh chefsolo@11.208.33.64"
alias sbkube3="echo password = solochef && echo expire date = 04-31-2018 && ssh chefsolo@11.208.33.32"

alias sbdse="echo password = solochef && ssh chefsolo@11.208.33.3"
alias sbdse2="echo password = solochef && ssh chefsolo@11.208.33.14"
alias sbdse3="echo password = solochef && ssh chefsolo@11.208.32.248"
# jobs -l -> lists all jobs currently running/suspended
# kill -9 pid kills process
# kill `jobs -p` kills all jobs running
#
# also could use pgrep to get pid of service and then use kill pid 
# brew services list lists all services provided by brew
# brew services start mongodb
# brew services stop mongodb
# list all instances connected to local mongodb: $ lsof -n -i4TCP:27017
# docker run -e MYSQL_ROOT_PASSWORD=password -p 3306:3306 MYSQL_ROOT_PASSWORD
#
# # Kafka command lines tools
# docker run --rm -it --net=host landoop/fast-data-dev bash
#
# Change all directories to 755 and files to 644
# find ~/Workspace/learn/jhipster/Ex_Files_JHipster_SpringBoot_Micro -type d -exec chmod 755 {} \;
# find ~/Workspace/learn/jhipster/Ex_Files_JHipster_SpringBoot_Micro -type f -exec chmod 644 {} \;
#
# Create a new conda env named cassandra with no default packages
# conda create --no-default-packages -n cassandra python=3.6.4
# source activate cassandra
# source deactivate
#
# To remove container name
# docker rm cassandra
#
# To create virtualenv
# python -m venv venv 
#
# To activate virtualenv
# source venv/bin/activate 
#
# To deactivate 
# deactivate 

export JAVA_HOME=$JAVA8_172_HOME
export SPARK_HOME=/usr/local/opt/spark-2.3.0-bin-hadoop2.7
export ETL_WORKSPACE=~/Workspace/ifscap/ecom-dboard-etl
export PYTHONUNBUFFERED=1

export SCALA_HOME=/usr/local/opt/scala/idea 
export GRAPHLOADER_HOME=/usr/local/opt/dse-graph-loader-6.0.0
export PIP_REQUIRE_VIRTUALENV=true
export CONDA_ENV_PATH=$ANACONDA_HOME
export JULIA_HOME=/Applications/Julia-0.6.app/Contents/Resources/julia
export ANALYTICS_DIR=~/Workspace/ifscap/analytics
export ANALYTICS_PARENT=~/Workspace/ifscap
export ANALYTICS_PYTHONHOME=$ANACONDA_HOME/bin
export MAIL=/usr/bin/mail 
export ANALYTICS_DC=jc
export ISTIO_HOME=/usr/local/opt/istio-0.5.0
export GOPATH=~/Workspace/go 
export GOBIN=$GOPATH/bin
export JMETER_HOME=/usr/local/opt/apache-jmeter-3.3
export INTELLIJ_HOME=/Applications/IntelliJ\ IDEA.app/Contents
export PATH=$ANACONDA_HOME/bin:$PATH:$GOPATH/bin:$MONGOOSE_HOME:/$JMETER_HOME/bin:$ISTIO_HOME/bin:/usr/texbin:$INTELLIJ_HOME/MacOS
export PATH=$PATH:$GATLING_HOME/bin:$SPARK_HOME/bin:$GRAPHLOADER_HOME
# Fixes weird terminal clear error
export TERMINFO=/usr/share/terminfo

prompt_context() {}
export HISTFILESIZE=1000000000
export HISTSIZE=1000000
alias config='/usr/bin/git --git-dir=/Users/b004917/.cfg/ --work-tree=/Users/b004917'
